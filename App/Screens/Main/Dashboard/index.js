import React, { Component } from 'react'
import { View } from 'react-native'
import { Text, Container, Button } from '@Components'
import Translations from '@Translations'
import { Images } from '@Theme'
import Firebase from '@Services/Firebase'
import Navigation from '@Services/Navigation'

export default class Dashboard extends Component {
  static options() {
    return {
      topBar: {
        height: 0,
        visible: false,
      },
    }
  }

  logout = async () => {
    await Firebase.signOut()
    Navigation.resetToOnboarding()
  }

  render() {
    return (
      <Container imageSource={Images.background} centered="all">
        <Text.T1>{Translations.Dashboard.title}</Text.T1>
        <Button long title={'Test Logout'} onPress={this.logout} />
      </Container>
    )
  }
}
