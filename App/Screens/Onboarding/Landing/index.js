import React, { Component } from 'react'
import { View, Image, ActivityIndicator, Alert } from 'react-native'
import { Images, Fonts, Colors, Icons } from '@Theme'
import Navigation from '@Services/Navigation'
import { Container, Button, Text } from '@Components'
import Translations from '@Translations'
import styles from './styles'
import Firebase from '@Services/Firebase'
import { showExistingDietListModal } from '@Common/Helpers'

class Landing extends Component {
  state = {
    isLoading: true,
  }

  componentDidMount = async () => {
    const alreadyHaveADietList = await Firebase.checkIfUserHaveADietList(true)
    if (!Firebase.currentUser) {
      return this.setState({ isLoading: false })
    }

    if (!alreadyHaveADietList) {
      return Navigation.push(this.props.componentId, 'NewDietList')
    }
    showExistingDietListModal(this.props.componentId)
  }

  render() {
    const { componentId } = this.props
    return (
      <Container androidPadStatusBar centered="all" imageSource={Images.background}>
        <View style={styles.topContainer}>
          <Text.Logo>{Translations.Onboarding.Landing.appName.toLocaleLowerCase()}</Text.Logo>
          <Text.T3D type={Fonts.family.gothamLight}>
            {Translations.Onboarding.Landing.appMotto}
          </Text.T3D>
        </View>

        <View style={styles.imageContainer}>
          <Image style={styles.image} source={Images.bowl} />
        </View>

        <View style={styles.bottomContainer}>
          {this.state.isLoading && (
            <ActivityIndicator style={{ paddingBottom: 50 }} size="large" color={Colors.PRIMARY} />
          )}
          {!this.state.isLoading && (
            <View>
              <Button
                long
                wide
                backgroundColor={Colors.SECONDARY}
                title={Translations.Onboarding.Landing.startNew}
                onPress={() => {}}
              />

              <Button
                icon={Icons.arrowRight}
                long
                title={Translations.Onboarding.Landing.login}
                onPress={() => {}}
              />
            </View>
          )}
        </View>
      </Container>
    )
  }
}

export default Landing
