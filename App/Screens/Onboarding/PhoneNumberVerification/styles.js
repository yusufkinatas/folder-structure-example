import { StyleSheet } from 'react-native'
import { Screen, Colors, Fonts } from '@Theme'
export default StyleSheet.create({
  middleContainer: {
  },
  topOfButtonContainer: {
  },
  topOfButtonContainerForVerifcation: {
  },
  codeDigitOuterContainer: {
  },
  codeDigitInnerContainer: {
  },
  button: {
  },
  phoneInput: {
  },
  horizontalLine: {  },
  verticalLine: {
  },
})
