import React, { Component } from 'react'
import { TextInput, View, Alert, Keyboard } from 'react-native'
import CountryPicker, { getAllCountries } from 'react-native-country-picker-modal'
import DeviceInfo from 'react-native-device-info'
import { Container, Text } from '@Components'
import validator from 'validator'
import Firebase from '@Services/Firebase'
import Navigation from '@Services/Navigation'
import { Images, Colors, Fonts } from '@Theme'
import TopContainer from '@Containers/Onboarding/TopContainer'
import VerificationButton from './VerificationButton'
import styles from './styles'
import Translations from '@Translations'
import { showExistingDietListModal } from '@Common/Helpers';

const VERIFICATION_CODE_LENGTH = 6

export default class PhoneNumberVerification extends Component {
  static options() {
    return {
      topBar: {
        height: 0,
        visible: false,
      },
    }
  }

  constructor(props) {
    super(props)
    let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
    const userCountryData = getAllCountries()
      .filter((country) => country.cca2 === userLocaleCountryCode)
      .pop()
    let callingCode = null
    let cca2 = userLocaleCountryCode
    if (!cca2 || !userCountryData) {
      cca2 = 'US'
      callingCode = '1'
    } else {
      callingCode = userCountryData.callingCode
    }
    this.state = {
      cca2: 'TR',
      callingCode: '90',
      phoneNumber: '5394505722',
      // cca2: cca2,
      // callingCode: callingCode,
      // phoneNumber: '',
      verificationCode: '',
      inInputPhase: true,
      isLoading: false,
    }
  }

  componentWillMount() {
    this.unsubscribe = Firebase.onAuthStateChanged((user) => {
      if (user) {
        console.log('User is authenticated!')
        this.onPhoneNumberVerificated()
      }
    })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  createPhoneNumberString = () => `+${this.state.callingCode}${this.state.phoneNumber}`

  onChangePhoneNumber = (text) => {
    this.setState({ phoneNumber: text })
  }

  onChangeVerificationCode = (text) => {
    this.setState({ verificationCode: text })
  }

  onPhoneNumberVerificated = async () => {
    let goToNewDietList = () => Navigation.push(this.props.componentId, 'NewDietList')
    try {
      //kayıtlı diyet listesi kontrolü
      const alreadyHaveADietList = await Firebase.checkIfUserHaveADietList(false)
      if (!alreadyHaveADietList) {
        //kayıtlı diyet listesi yok
        return goToNewDietList()
      }
      //kayıtlı diyet listesi var
      showExistingDietListModal(this.props.componentId)
    } catch (error) {
      throw new Error(error)
    }
  }

  tryToSendVerificationCode = async () => {
    let phoneNumber = this.createPhoneNumberString()
    if (!validator.isMobilePhone(phoneNumber)) {
      return
    }
    try {
      this.setState({ isLoading: true })
      const confirmationResult = await Firebase.signInWithPhoneNumber(phoneNumber)
      this.setState({ isLoading: false, inInputPhase: false })
      this.confirmationResult = confirmationResult
    } catch (error) {
      this.setState({ isLoading: false })
      alert(Translations.Errors.unexpected)
      console.log('ERROR', error)
    }
  }

  tryToConfirmVerificationCode = async () => {
    try {
      this.setState({ isLoading: true })
      let user = await this.confirmationResult.confirm(this.state.verificationCode)
      this.setState({ isLoading: false })
    } catch (error) {
      this.setState({ isLoading: false })
      alert(Translations.Errors.unexpected)
      console.log('ERROR', error)
    }
  }

  renderPhoneInput = () => {
    const { callingCode, phoneNumber } = this.state
    return (
      <View style={styles.topOfButtonContainer}>
        <CountryPicker
          onChange={(value) => {
            this.setState({ cca2: value.cca2, callingCode: value.callingCode })
          }}
          cca2={this.state.cca2}
          flagType="flat"
        />
        <Text.T2G type={Fonts.family.book} style={{ marginLeft: 10 }}>
          +{callingCode}
        </Text.T2G>

        <View style={styles.verticalLine} />

        <TextInput
          onChangeText={this.onChangePhoneNumber}
          value={phoneNumber}
          placeholder={Translations.Onboarding.PhoneNumberVerification.phoneNumber}
          placeholderTextColor={Colors.GREY_DARK}
          keyboardType="phone-pad"
          style={styles.phoneInput}
          autoFocus={true}
        />
      </View>
    )
  }

  renderVerificationCodeDigit = (index) => {
    const { verificationCode } = this.state
    let number = verificationCode[index]
    return (
      <View key={index} style={styles.codeDigitOuterContainer}>
        <View style={styles.codeDigitInnerContainer}>
          {typeof number == 'string' ? (
            <Text.T1 type={Fonts.family.book} style={{ color: Colors.TERTIARY }}>
              {number}
            </Text.T1>
          ) : (
            <View style={styles.horizontalLine} />
          )}
        </View>
      </View>
    )
  }

  onVerificationCodeTouch = () => {
    console.log('isFocused', this.codeInput.isFocused())
    if (this.codeInput.isFocused()) {
      return this.codeInput?.blur()
    }
    this.codeInput?.focus()
  }

  renderVerification = () => {
    let digits = []
    for (let i = 0; i < VERIFICATION_CODE_LENGTH; i++) {
      digits.push(this.renderVerificationCodeDigit(i))
    }
    return (
      <View
        style={styles.topOfButtonContainerForVerifcation}
        onTouchStart={this.onVerificationCodeTouch}
      >
        {digits}
        <TextInput
          onChangeText={this.onChangeVerificationCode}
          value={this.state.verificationCode}
          keyboardType="phone-pad"
          maxLength={VERIFICATION_CODE_LENGTH}
          ref={(r) => (this.codeInput = r)}
          autoFocus
          style={{ width: 0, height: 0, position: 'absolute' }}
        />
      </View>
    )
  }

  render() {
    const { callingCode, phoneNumber, verificationCode, inInputPhase, isLoading } = this.state
    return (
      <Container androidPadStatusBar imageSource={Images.background} centered="horizontal">
        <TopContainer />

        <Text.T2 type={Fonts.family.quicksandMedium}>
          {inInputPhase
            ? Translations.Onboarding.PhoneNumberVerification.enterYourNumber
            : Translations.Onboarding.PhoneNumberVerification.verificationCode}
        </Text.T2>

        <View style={styles.middleContainer}>
          <VerificationButton
            isLoading={isLoading}
            disabled={
              isLoading || inInputPhase
                ? !validator.isMobilePhone(this.createPhoneNumberString())
                : verificationCode.length != VERIFICATION_CODE_LENGTH
            }
            onPress={
              inInputPhase ? this.tryToSendVerificationCode : this.tryToConfirmVerificationCode
            }
          />
          {inInputPhase ? this.renderPhoneInput() : this.renderVerification()}
        </View>
      </Container>
    )
  }
}
