import React from 'react'
import { TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import styles from './styles'
import { Icon } from '@Components'
import { Icons, Screen, Colors } from '@Theme'

const VerificationButton = ({ onPress, disabled, isLoading }) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress} style={styles.button}>
      {isLoading ? (
        <ActivityIndicator size="large" color={Colors.WHITE} />
      ) : (
        <Icon source={Icons.longArrowRight} width={Screen.width * 0.13} height={40} />
      )}
    </TouchableOpacity>
  )
}

export default VerificationButton
