import React, { Component } from 'react'
import { View, ActivityIndicator } from 'react-native'
import { Colors, Images, Icons, Fonts, Screen } from '@Theme'
import Navigation from '@Services/Navigation'
import { Button, Container, Text } from '@Components'
import Firebase from '@Services/Firebase'
import Translations from '@Translations'
import styles from './styles'
import TopContainer from '@Containers/Onboarding/TopContainer'
import { showExistingDietListModal } from '@Common/Helpers'

export default class Login extends Component {
  static options() {
    return {
      topBar: {
        height: 0,
        visible: false,
      },
    }
  }

  state = {
    isLoading: false,
  }

  signInWithFacebook = async () => {
    this.setState({ isLoading: true })
    const firebaseUserCredential = await Firebase.signInWithFacebook()

    if (!firebaseUserCredential) {
      return this.setState({ isLoading: false })
    }
    const alreadyHaveADietList = await Firebase.checkIfUserHaveADietList(true)
    if (!Firebase.currentUser) {
      return this.setState({ isLoading: false })
    }

    if (!alreadyHaveADietList) {
      this.setState({ isLoading: false })
      return Navigation.push(this.props.componentId, 'NewDietList')
    }
    showExistingDietListModal(this.props.componentId)
  }

  render() {
    return (
      <Container androidPadStatusBar imageSource={Images.background} centered="horizontal">
        <TopContainer />

        <View style={styles.middleContainer}>
          <Text.T3 centereds type={Fonts.family.gothamLight}>
            {Translations.Onboarding.Login.dontWorry}
          </Text.T3>
        </View>

        <Button
          icon={Icons.facebook}
          iconOnBottomBorder
          backgroundColor={Colors.FACEBOOK}
          long
          loading={this.state.isLoading}
          title={Translations.Onboarding.Login.withFacebook}
          onPress={this.signInWithFacebook}
        />
        <Button
          icon={Icons.phoneHalf}
          iconOnBottomBorder
          long
          title={Translations.Onboarding.Login.withMobileNumber}
          onPress={() => Navigation.push(this.props.componentId, 'PhoneNumberVerification')}
        />
      </Container>
    )
  }
}
