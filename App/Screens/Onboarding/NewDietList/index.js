import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Images } from '@Theme'
import Navigation from '@Services/Navigation'
import { Button, Container } from '@Components'
import Firebase from '@Services/Firebase'

export default class Login extends Component {
  static options() {
    return {
      topBar: {
        height: 0,
        visible: false,
      },
    }
  }

  state = {
    isLoading: false,
  }

  onDialogFinished = async () => {
    if (!Firebase.currentUser) {
      return Navigation.push(this.props.componentId, 'Login')
    }
    Navigation.startMainScene()
  }

  render() {
    return (
      <Container imageSource={Images.background} centered="all">
        <Button
          isLoading={this.state.isLoading}
          long
          title="test"
          onPress={this.onDialogFinished}
        />
      </Container>
    )
  }
}

const styles = StyleSheet.create({})
