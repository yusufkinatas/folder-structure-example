import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import { Colors, Screen, Images } from '@Theme'
import { GiftedChat } from 'react-native-gifted-chat'
import uuid from 'uuid/v1'

const user = {
  _id: 1,
}
const dietBot = {
  _id: 2,
  name: 'Dietvise Bot',
  avatar: 'https://placeimg.com/140/140/any',
}

export default class Landing extends Component {
  state = {
    messages: [],
  }

  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 124,
          text: 'Choose your favotite',
          createdAt: new Date(),
          user: dietBot,
          quickReplies: {
            type: 'radio', // or 'checkbox',
            values: [
              {
                title: 'White meat',
                value: 'yes',
              },
              {
                title: 'Red meat',
                value: 'no',
              },
            ],
          },
        },
      ],
    })
  }

  onSend = (messages = []) => {
    console.log(messages)
    this.setState({ messages: GiftedChat.append(this.state.messages, messages) })
  }

  onQuickReply = (replies) => {
    if (!replies) {
      return
    }
    let message = {
      user: user,
      text: '',
      createdAt: new Date(),
      _id: uuid(),
    }
    replies.forEach(
      (reply, index) => (message.text += index == 0 ? reply.title : `, ${reply.title}`)
    )
    console.log(message.text)
    this.onSend(message)
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          marginBottom: Screen.isIphoneX ? 34 : 0,
          marginTop: Screen.isIphoneX ? 44 : 0,
        }}
      >
        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          onQuickReply={this.onQuickReply}
          user={user}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({})
