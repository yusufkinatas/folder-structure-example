export default (generateLang = (json) => {
  return {
    Errors: {
      unexpected: 'test',
    },
    Onboarding: {
      Landing: {
        appName: 'test',
        appMotto: 'test',
        startNew: 'test',
        login: 'test',
      },
      Login: {
        dontWorry: 'test',
        withFacebook: 'test',
        withMobileNumber: 'test',
      },
      PhoneNumberVerification: {
        phoneNumber: 'test',
        verificationCode: 'test'
      },
    },
    Dashboard: {
      title: 'test',
    },
  }
})
