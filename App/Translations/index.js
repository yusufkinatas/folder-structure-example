import LocalizedStrings from 'react-native-localization'
import generateLanguage from './generateLanguage'

//projede gömülü bulunan diller
var languages = {
  en: require('./en.json'),
}

//en az bir dili manuel olarak tanımlamazsak autoComplete özelliği çalışmıyor
var langObject = {
  en: generateLanguage(languages['en']),
}

//projede bulunan dillerin dil objesine eklenmesi
Object.keys(languages).forEach((lang) => {
  langObject[lang] = generateLanguage(languages[lang])
})

var Translations = new LocalizedStrings(langObject)

export default Translations
