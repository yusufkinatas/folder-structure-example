export default {
  T1: {
    fontSize: 20,
  },
  T2: {
    fontSize: 18,
  },
  T3: {
    fontSize: 16,
  },
  T4: {
    fontSize: 14,
  },
  T5: {
    fontSize: 12,
  },
}
