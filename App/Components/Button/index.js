import { Text } from '@Components'
import React from 'react'
import { View, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import styles from './styles'
import { Screen, Fonts, Colors } from '@Theme'
import Icon from '@Components/Icon'

const Button = ({
  disabled,
  title,
  onPress,
  icon,
  iconOnBottomBorder,
  long,
  wide,
  backgroundColor,
  loading,
}) => {
  let containerStyle = disabled ? styles.containerDisabled : styles.container
  let height = wide ? 80 : 50
  return (
    <TouchableOpacity
      disabled={disabled || loading}
      style={{
        ...containerStyle,
        backgroundColor: backgroundColor || Colors.PRIMARY,
        justifyContent: 'center',
        width: long ? Screen.width * 0.9 : undefined,
        height,
      }}
      onPress={onPress}
    >
      {icon && (
        <View
          style={{
            position: 'absolute',
            left: 20,
            height: 50,
            justifyContent: iconOnBottomBorder ? 'flex-end' : 'center',
          }}
        >
          <Image source={icon} style={{ height: 40, width: 30 }} resizeMode="contain" />
        </View>
      )}
      {loading ? (
        <ActivityIndicator color={Colors.WHITE} />
      ) : disabled ? (
        <Text.T3G bold>{title}</Text.T3G>
      ) : (
        <Text.T3W bold>{title}</Text.T3W>
      )}
    </TouchableOpacity>
  )
}

export default Button
