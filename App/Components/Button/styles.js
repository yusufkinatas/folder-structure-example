import { StyleSheet } from 'react-native'
import { Colors } from '@Theme'

const commonContainerStyle = {
  display: 'flex',
  alignItems: 'center',
  marginVertical: 10,
  borderRadius: 22,
  elevation: 5,
  shadowColor: Colors.BLACK,
  shadowOffset: { height: 3, width: 3 },
  shadowOpacity: 0.3,
  shadowRadius: 5,
  elevation: 3,
}

export default StyleSheet.create({
  container: {
    ...commonContainerStyle,
    backgroundColor: Colors.PRIMARY,
  },
  containerDisabled: {
    ...commonContainerStyle,
    backgroundColor: Colors.GREY,
  },
})
