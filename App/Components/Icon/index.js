import React from 'react'
import { Image } from 'react-native'
import PropTypes from 'prop-types'
import styles from './styles'

const Icon = ({ source, width, height, color }) => {
  return (
    <Image
      tintColor={color}
      source={source}
      resizeMode="contain"
      style={[
        styles.image,
        {
          tintColor: color,
          width: width,
          height: height,
        },
      ]}
    />
  )
}

Icon.propTypes = {
  color: PropTypes.string,
  source: PropTypes.number.isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
}

Icon.defaultProps = {
  width: 100,
  height: 100,
}

export default Icon
