const family = {
  medium: 'CircularStd-Medium',
  mediumItalic: 'CircularStd-MediumItalic',
  black: 'CircularStd-Black',
  blackItalic: 'CircularStd-BlackItalic',
  bold: 'CircularStd-Bold',
  boldItalic: 'CircularStd-BoldItalic',
  book: 'CircularStd-Book',
  bookItalic: 'CircularStd-BookItalic',
  
  gothamBlack: 'Gotham-Black',
  gothamBold: 'Gotham-Bold',
  // gothamBook: 'Gotham-Book',
  gothamBookItalic: 'Gotham-BookItalic',
  gothamLight: 'Gotham-Light',
  gothamThin: 'Gotham-Thin',
  gothamThinItalic: 'Gotham-ThinItalic',
  gothamUltraItalic: 'Gotham-UltraItalic',
  gothamXLight: 'Gotham-XLight',
  gothamXLightItalic: 'Gotham-XLightItalic',
  
  quicksandBold: 'Quicksand-Bold',
  quicksandLight: 'Quicksand-Light',
  quicksandMedium: 'Quicksand-Medium',
  quicksandRegular: 'Quicksand-Regular',
}

export default {
  family,
}
