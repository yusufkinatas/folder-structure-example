export default {
  PRIMARY: '#FF552B',
  SECONDARY: '#23B1D3',
  TERTIARY: '#FFE5CC',
  FACEBOOK: '#2962D7',
  DARKER: '#252525',
  DARK: '#4B4342',
  WHITE: '#FFFFFF',
  GREY_DARK: '#A9A9A9',
  GREY: '#DCDCDC',
  GREY_LIGHT: '#EEEEEE',
  GREY_LIGHTEST: '#F5F5F5',
  TRANSPARENT: 'transparent',
  BLACK: '#000000',
}
