export default {
  arrowRight: require('@Assets/icons/arrowRight/arrowRight.png'),
  longArrowRight: require('@Assets/icons/longArrowRight/longArrowRight.png'),
  facebook: require('@Assets/icons/facebook/facebook.png'),
  phoneHalf: require('@Assets/icons/phoneHalf/phoneHalf.png'),
}
