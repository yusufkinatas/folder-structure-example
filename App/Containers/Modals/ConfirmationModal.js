import React, { Component } from 'react'
import { Navigation } from 'react-native-navigation'
import { View, StyleSheet, TouchableOpacity, Animated } from 'react-native'
import { Screen, Colors, Fonts } from '@Theme'
import { Text } from '@Components'

export default class AboutUsModal extends Component {
  constructor(props) {
    super(props)
    this.anim = new Animated.Value(0)
  }

  componentWillMount() {
    Animated.timing(this.anim, {
      duration: 250,
      toValue: 1,
      useNativeDriver: true,
    }).start()
  }

  dissmissModal = (onDismissed) => {
    Animated.timing(this.anim, {
      duration: 250,
      toValue: 0,
      useNativeDriver: true,
    }).start(() => {
      Navigation.dismissModal(this.props.componentId)
      onDismissed()
    })
  }

  onButtonPressed = () => {}

  render() {
    console.log('PROPS', this.props)

    const {
      dismissable,
      onConfirm,
      onReject,
      title,
      description,
      rejectTitle,
      confirmTitle,
    } = this.props
    return (
      <Animated.View
        style={{
          ...styles.outerContainer,
          opacity: this.anim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
          }),
        }}
      >
        <View style={styles.touchableArea} onTouchStart={dismissable && this.dissmissModal} />
        <Animated.View
          style={{
            ...styles.innerContainer,
            transform: [
              {
                translateY: this.anim.interpolate({
                  inputRange: [0, 1],
                  outputRange: [Screen.height * 0.15, 0],
                }),
              },
            ],
          }}
        >
          <Text.T1D type={Fonts.family.gotham} style={{ marginBottom: 10 }} centered>
            {title}
          </Text.T1D>
          <Text.T4D>{description}</Text.T4D>
          <View style={{ width: '100%', paddingBottom: Screen.isIphoneX ? 34 : 0 }}>
            <TouchableOpacity
              style={{ ...styles.button, backgroundColor: Colors.GREY_DARK }}
              onPress={() => this.dissmissModal(onReject)}
            >
              <Text.T3W bold>{rejectTitle}</Text.T3W>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => this.dissmissModal(onConfirm)}>
              <Text.T3W bold>{confirmTitle}</Text.T3W>
            </TouchableOpacity>
          </View>
        </Animated.View>
      </Animated.View>
    )
  }
}

var styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  innerContainer: {
    width: Screen.width,
    paddingVertical: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  touchableArea: {
    zIndex: -1,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    position: 'absolute',
  },
  button: {
    height: 50,
    backgroundColor: Colors.PRIMARY,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
})
