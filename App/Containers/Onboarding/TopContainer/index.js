import React from 'react'
import { View, Image } from 'react-native'

import { Images } from '@Theme'
import Translations from '@Translations'
import { Text } from '@Components'
import styles from './styles'

const TopContainer = () => {
  return (
    <View style={styles.topContainer}>
      <Text.Logo>{Translations.Onboarding.Landing.appName.toLocaleLowerCase()}</Text.Logo>
      <View style={styles.imageContainer}>
        <Image source={Images.relax} style={styles.image} />
      </View>
    </View>
  )
}

export default TopContainer
