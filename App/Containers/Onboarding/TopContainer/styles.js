import { StyleSheet } from 'react-native'
import { Screen } from '@Theme'

export default StyleSheet.create({
  topContainer: {
    height: Screen.height * 0.3,
    width: '100%',
    alignItems: 'center',
  },
  imageContainer: {
    flex: 1,
    width: '75%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
})
