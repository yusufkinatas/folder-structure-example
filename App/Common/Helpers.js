import Firebase from '@Services/Firebase'
import Navigation from '@Services/Navigation'

export const startInitialization = () => {
  Firebase.initializeApp()
}

export const showExistingDietListModal = (componentId) => {
  Navigation.showModal('ConfirmationModal', {
    dismissable: false,
    onConfirm: Navigation.startMainScene,
    onReject: () => Navigation.push(componentId, 'NewDietList'),
    title: 'test',
    description: 'test',
    rejectTitle: 'test',
    confirmTitle: 'test',
  })
}
