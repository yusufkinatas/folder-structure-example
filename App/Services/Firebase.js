import firebase from 'react-native-firebase'
import { AccessToken, LoginManager } from 'react-native-fbsdk'
import { Platform } from 'react-native'
import EventEmitter from 'EventEmitter'

var app = null

const Firebase = {
  initializeApp: async () => {
    const iosConfig = {
      clientId: '932602569161-3vri23l9l48q53fisqh7sd5jjdq480e5.apps.googleusercontent.com',
      appId: '1:932602569161:ios:cc9d76ef86f7336f',
      apiKey: 'AIzaSyBrApJRH8uLEvRi35Oa0wv6E-vM69_1lmw',
      databaseURL: 'https://dietvise-test.firebaseio.com',
      storageBucket: 'dietvise-test.appspot.com',
      messagingSenderId: '932602569161',
      projectId: 'dietvise-test',
      persistence: true,
    }

    const androidConfig = {
      clientId: '932602569161-3vri23l9l48q53fisqh7sd5jjdq480e5.apps.googleusercontent.com',
      appId: '1:932602569161:android:2b07531648689f99',
      apiKey: 'AIzaSyAMHwFa5ExUmY0Jg-6-zdhuT_N4KuD_JDU',
      databaseURL: 'https://dietvise-test.firebaseio.com',
      storageBucket: 'dietvise-test.appspot.com',
      messagingSenderId: '932602569161',
      projectId: 'dietvise-test',
      persistence: true,
    }

    const dietviseApp = firebase.initializeApp(
      Platform.OS === 'ios' ? iosConfig : androidConfig,
      'dietvise'
    )

    try {
      app = await dietviseApp.onReady()
      console.log('Firebase init\nUser:', app.auth().currentUser)
    } catch (error) {
      throw new Error(error)
    }
  },

  get currentUser() {
    return app.auth().currentUser
  },

  signInWithPhoneNumber: async (phoneNumber) => {
    try {
      const confirmationResult = await app.auth().signInWithPhoneNumber(phoneNumber, true)
      return confirmationResult
    } catch (error) {
      throw new Error(error)
    }
  },

  signOut: async () => {
    await app.auth().signOut()
  },

  onAuthStateChanged: (callback) => {
    return app.auth().onAuthStateChanged(callback)
  },

  checkIfUserHaveADietList: async (testValue) => {
    await new Promise((res) => setTimeout(res, 500))
    return testValue
  },

  signInWithFacebook: async () => {
    try {
      const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email'])

      if (result.isCancelled) {
        // handle this however suites the flow of your app
        console.log('User cancelled request')
      }

      console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`)

      const data = await AccessToken.getCurrentAccessToken()
      if (!data) {
        // handle this however suites the flow of your app
        console.log('Something went wrong obtaining the users access token')
      }

      // create a new firebase credential with the token
      const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken)

      // login with credential
      const firebaseUserCredential = await app.auth().signInWithCredential(credential)
      return firebaseUserCredential
      console.warn(JSON.stringify(firebaseUserCredential.user.toJSON()))
    } catch (e) {
      console.log('Error', e)
    }
  },

  // checkIfUserExistsWithPhoneNumber: async (phoneNumber) => {
  //   try {
  //     const snapshot = await firebase
  //       .database()
  //       .ref('/users')
  //       .orderByChild('phoneNumber')
  //       .equalTo(phoneNumber)
  //       .once('value')

  //     console.log('Snapshot', snapshot.val())
  //     if (!snapshot.val()) {
  //       return false
  //     }
  //     return true
  //   } catch (error) {
  //     throw new Error(error)
  //   }
  // },

  // saveUserToDatabase: async (user) => {
  //   const { uid, phoneNumber } = user
  //   firebase
  //     .database()
  //     .ref('/users')
  //     .update({
  //       [uid]: {
  //         phoneNumber,
  //       },
  //     })
  // },
}

export default Firebase
