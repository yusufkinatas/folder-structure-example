import { Navigation } from 'react-native-navigation'

import Landing from '@Screens/Onboarding/Landing'
import Login from '@Screens/Onboarding/Login'
import NewDietList from '@Screens/Onboarding/NewDietList'
import PhoneNumberVerification from '@Screens/Onboarding/PhoneNumberVerification'

import Dashboard from '@Screens/Main/Dashboard'

import ConfirmationModal from '@Containers/Modals/ConfirmationModal'
import Playground from '@Screens/Playground'

export default () => {
  Navigation.registerComponent('Landing', () => Landing)
  Navigation.registerComponent('Login', () => Login)
  Navigation.registerComponent('PhoneNumberVerification', () => PhoneNumberVerification)
  Navigation.registerComponent('NewDietList', () => NewDietList)

  Navigation.registerComponent('Dashboard', () => Dashboard)

  Navigation.registerComponent('ConfirmationModal', () => ConfirmationModal)
  Navigation.registerComponent('Playground', () => Playground)
}
