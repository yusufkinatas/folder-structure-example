import { Navigation } from 'react-native-navigation'
import { Platform } from 'react-native'
import registerScreens from './registerScreens'
import setDefaultNavigationOptions from './setDefaultNavigationOptions'

const PUSH_COOLDOWN = 250
var pushAviable = true

export default {
  registerScreens,

  push: (componentId, screen, passProps) => {
    if (!pushAviable) return
    Navigation.push(componentId, {
      component: {
        name: screen,
        options: {},
        passProps,
      },
    })
    pushAviable = false
    setTimeout(() => {
      pushAviable = true
    }, PUSH_COOLDOWN)
  },

  showModal: (screen, passProps) => {
    let isAndroid = Platform.OS == 'android'
    Navigation.showModal({
      component: {
        name: screen,
        passProps,
        options: {
          layout: {
            backgroundColor: 'transparent',
          },
          modalPresentationStyle: isAndroid ? 'overCurrentContext' : 'overFullScreen',
        },
      },
    })
  },

  startOnboarding: () => {
    Navigation.events().registerAppLaunchedListener(() => {
      setDefaultNavigationOptions()
      Navigation.setRoot({
        root: {
          stack: {
            children: [
              {
                component: {
                  name: 'Landing',
                  // name: 'Playground',
                  options: {
                    topBar: {
                      height: 0,
                      visible: false,
                    },
                  },
                },
              },
            ],
          },
        },
      })
    })
  },

  resetToOnboarding: () => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: 'Landing',
                options: {
                  topBar: {
                    height: 0,
                    visible: false,
                  },
                },
              },
            },
          ],
        },
      },
    })
  },

  startMainScene: () => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: 'Dashboard',
                options: {
                  // topBar: { visible: false },
                },
              },
            },
          ],
        },
      },
    })
  },
}
