import { Navigation } from 'react-native-navigation'
import { StatusBar } from 'react-native'
import { Colors, Screen, Fonts } from '@Theme'

export default () => {
  Navigation.setDefaultOptions({
    topBar: {
      topMargin: StatusBar.currentHeight,
      background: {
        color: Colors.GREY_LIGHT,
      },
      backButton: {
        showTitle: false,
      },
    },
    statusBar: {
      drawBehind: true,
      backgroundColor: 'transparent',
      style: 'dark',
    },
    layout: {
      orientation: ['portrait'],
    },
    animations: {
      setRoot: {
        alpha: {
          from: 0,
          to: 1,
          duration: 250,
        },
      },
      showModal: {
        enabled: false,
      },
      dismissModal: {
        enabled: false,
      },
      // push: {
      //   content: {
      //     alpha: {
      //       from: 0,
      //       to: 1,
      //       duration: 250,
      //       interpolation: 'accelerate',
      //     },
      //     y: {
      //       from: Screen.height * 0.15,
      //       to: 0,
      //       duration: 250,
      //       interpolation: 'accelerate',
      //     },
      //   },
      // },
      // pop: {
      //   content: {
      //     alpha: {
      //       from: 1,
      //       to: 0,
      //       duration: 250,
      //       interpolation: 'decelerate',
      //     },
      //     y: {
      //       from: 0,
      //       to: Screen.height * 0.15,
      //       duration: 250,
      //       interpolation: 'decelerate',
      //     },
      //   },
      // },
    },
  })
}
