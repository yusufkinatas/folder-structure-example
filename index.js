import Navigation from '@Services/Navigation'
import { startInitialization } from '@Common/Helpers'

Navigation.registerScreens()
Navigation.startOnboarding()
startInitialization()

console.disableYellowBox = true
